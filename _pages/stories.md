---
layout: page
title: PGHK stories
title-zh: 庇能福建の故事
permalink: /stories/
css: pages
---
<ul class="l-shelf c-shelf">
  {% for post in site.posts %}
  <a class="l-story c-story" href="{{ post.url | prepend: site.baseurl }}">
    <li>
      <p>{{ post.title }}</p>
    </li>
  </a>
  {% endfor %}
</ul>
