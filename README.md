# Penang Hokkien | 槟城福建话

The website is built on Jekyll, and makes use of gulp as its task runner. The site is hosted using [GitLab Pages](https://pages.gitlab.io/).

Dependencies on [Ruby](https://www.ruby-lang.org/en/) for [Bundler](http://bundler.io/) and [Node.js](https://nodejs.org/en/) for [npm](https://www.npmjs.com/) or [yarn](https://yarnpkg.com/). Local development setup is as follows:

1. `git clone git@github.com:huijing/pghk.git`
2. `bundle install`
2. `npm install`
3. `gulp`

- Styles go in the _sass folder, and will be compiled by Gulp
- Scripts go into the _js folder, and will be concatenated by Gulp

## Background

This site is built to be fully responsive, and to make use of the CSS writing-mode property to create layouts that integrate both horizontal and vertical scripts in one cohesive design.

Due to differences in browser rendering, only Chrome displays correctly. Browser-specific layout issues are being tracked in [the issue log](https://gitlab.com/penang-hokkien/penang-hokkien.gitlab.io/issues).

## Contribute

If you are interested in contributing to this project, feel free to submit a pull request.
